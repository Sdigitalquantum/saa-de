@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')
      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Master Barang</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            <br>	
            <div class="text-right mg-r-25">
            	 <a href="{{ route('master_customer_create') }}" class="btn waves-effect waves-light btn-md btn-success"><i class="fa fa-plus"></i> Add Data</a>
            </div>
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
		            <table id="datatable1" class="table display responsive nowrap">
		              <thead>
		                <tr>
		                  <th class="wd-15p">Kode</th>
		                  <th class="wd-15p">Nama</th>
		                  <th class="wd-20p">Tgl lahir</th>
		                  <th class="wd-20p">Alamat</th>
		                  <th class="wd-15p">Kota</th>
		                  <th class="wd-15p">Tlp</th>
		                  <th class="wd-15p">nama_customer2</th>
		                  <th class="wd-15p">Fax</th>
		                  <th class="wd-15p">Email</th>
		                  <th class="wd-25p">Action</th>
		                </tr>
		              </thead>
		              <tbody>
		              	@foreach ($data as $element)
		                <tr>
		                  <td>{{ $element->Kd_Customer }}</td>
		                  <td>{{ $element->Nama_Customer }}</td>
		                  <td>{{ date('d M Y',strtotime($element->Tgl_Lahir)) }}</td>
		                  <td>{{ $element->Alamat1 }}</td>
		                  <td>{{ $element->Kota1 }}</td>
		                  <td>{{ $element->No_Telepon1 }}</td>
		                  <td>{{ $element->nama_customer2 }}</td>
		                  <td>{{ $element->No_Fax }}</td>
		                  <td>{{ $element->Email }}</td>
		                  <td>
	                        <button class="btn btn-xm btn-primary"><i class="fa fa-pencil"></i> </button>  
	                        <button class="btn btn-xm btn-danger"><i class="fa fa-times"></i></button>  
	                      </td>
		                </tr>
		              	@endforeach
		              </tbody>
		            </table>
		          </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>


    @endsection

@section('extra_scripts')

<script>
</script>
    @endsection

       