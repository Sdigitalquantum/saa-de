@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')
      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Form Data</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
			      <form class="save">
		          <div class="form-layout form-layout-1">
			            <div class="row ">
			           	  <label class="col-sm-2 form-control-label">Kode Customer</label>
		                  <div class="col-sm-4 mg-b-5 mg-sm-t-0">
			                  <input class="form-control" type="text" name="cust_code" value=""  placeholder="Auto Gen">
		                  </div>
		                  <label class="col-sm-2 form-control-label">Tgl Lahir</label>
		                  <div class="col-sm-4 mg-b-5 mg-sm-t-0">
			                  <input class="form-control" type="date" name="cust_tgl_lahir" value=""  placeholder="Auto Gen">
		                  </div>

		                  <div class="col-sm-12 mg-b-5 mg-sm-t-0  "></div>

		                  <label class="col-sm-2 form-control-label mg-t-15">Nama Customer </label>
		                  <div class="col-sm-4 mg-b-5 mg-t-15">
			                  <input class="form-control" type="text" name="cust_name1"  placeholder="Nama Customer">
		                  </div>
		                  <label class="col-sm-2 form-control-label mg-t-15">Alamat</label>
		                  <div class="col-sm-4 mg-b-5 mg-t-15">
			                  <input class="form-control ref_po" type="text" name="cust_alamat1" placeholder="Alamat" >
		                  </div>
		                  <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
		                  <label class="col-sm-2 form-control-label">Kota</label>
		                  <div class="col-sm-4 mg-b-5 mg-sm-t-0">
			                  <input class="form-control" type="text" name="cust_kota1"  placeholder="Kota">
		                  </div>
		                  <label class="col-sm-2 form-control-label">Telp</label>
		                  <div class="col-sm-4 mg-b-5 mg-sm-t-0">
			                  <input class="form-control" type="text" name="cust_telp1"  placeholder="Telp">
		                  </div>

		                  <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>

		                  <label class="col-sm-2 form-control-label mg-t-15">Nama Customer 1</label>
		                  <div class="col-sm-4 mg-b-5 mg-t-15">
			                  <input class="form-control" type="text" name="cust_name2"  placeholder="Nama Customer 1">
		                  </div>
		                  <label class="col-sm-2 form-control-label mg-t-15">Alamat 1</label>
		                  <div class="col-sm-4 mg-b-5 mg-t-15">
			                  <input class="form-control ref_po" type="text" name="Alamat 1" >
		                  </div>
		                  <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
		                  <label class="col-sm-2 form-control-label">Kota 1</label>
		                  <div class="col-sm-4 mg-b-5 mg-sm-t-0">
			                  <input class="form-control" type="text" name="cust_kota2"  placeholder="Kota 1">
		                  </div>
		                  <label class="col-sm-2 form-control-label">Telp 1</label>
		                  <div class="col-sm-4 mg-b-5 mg-sm-t-0">
			                  <input class="form-control" type="text" name="cust_telp2"  placeholder="Telp 1">
		                  </div>


			            </div><!-- row -->

			        	<br>
		                {{-- <button class="btn btn-primary btnAdd btn-sm" type="button"><i class="fa fa-table"> </i> Add new</button> --}}
		                <a class="btn btn-info btn-icon rounded-circle mg-r-5 mg-b-10 save text-right" onclick="save()"><div><i style="color: white" class="fa fa-save"></i></div>
		                </a>
		            
		                 </form>
		         </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>


    @endsection

@section('extra_scripts')

<script>
    var table           = $("#t72a").DataTable({
    });
 
     function save() {

      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "get",
            url:'{{ route('order_in_save') }}',
            data: $('.save').serialize(),
            processData: false,
            contentType: false,
          success:function(data){
            if (data.status == 'sukses') {
               
            	alert('sukses');
                location.reload();
            }else if (data.status == 'ada') {
            	alert('sukses');
                
            }
          },error:function(){
            	alert('error');
            
            
          }
        });
        
    }

</script>
    @endsection

       