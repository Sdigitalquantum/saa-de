<!DOCTYPE html>
<html>
<head>
    <title></title>
    @include('layouts_backend._head_backend')
    @include('layouts_backend._css_backend')
    @yield('extra_styles')
</head>
<body>
	@include('layouts_backend._sidebar_left_backend')
	@include('layouts_backend._nav_backend')
	@include('layouts_backend._sidebar_right_backend')
    <div class="br-mainpanel">
	  @yield('content')
      <footer class="br-footer">
    		@include('layouts_backend._footer_backend')
      </footer>
    </div>

    @include('layouts_backend._scripts_backend')
    @yield('extra_scripts')
</body>
</html>