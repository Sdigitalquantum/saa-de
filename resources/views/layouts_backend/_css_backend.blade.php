<!-- vendor css -->
<link rel="stylesheet" href="{{ asset('assets/css/bracket.css') }}">

<link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
<link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
<link href="{{ asset('assets/lib/jquery-switchbutton/jquery.switchButton.css') }}" rel="stylesheet">
<link href="{{ asset('assets/lib/highlightjs/github.css') }}" rel="stylesheet">
{{-- <link href="{{ asset('assets/lib/rickshaw/rickshaw.min.css') }}" rel="stylesheet"> --}}
<link href="{{ asset('assets/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
<link href="{{ asset('assets/lib/select2/css/select2.min.css') }}" rel="stylesheet">
{{-- <link href="{{ asset('assets/lib/chartist/chartist.css') }}" rel="stylesheet"> --}}

<!-- Bracket CSS -->
