<div class="br-logo"><a href="{{ url('/home') }}"><span>[</span>SAA SYSTEM<span>]</span></a></div>
  <div class="br-sideleft overflow-y-auto">
      <label class="sidebar-label pd-x-15 mg-t-20">Navigasi</label>
      <div class="br-sideleft-menu">
        <a href="{{ url('/home') }}" class="br-menu-link active">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
            <span class="menu-item-label">Dashboard</span>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <a href="/Master" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
            <span class="menu-item-label">Master</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="{{ route('master_customer') }}" class="nav-link">Master Customer</a></li>
          <li class="nav-item"><a href="{{ route('master_supplier') }}" class="nav-link">Master Supplier</a></li>
          <li class="nav-item"><a href="{{ route('master_mata_uang') }}" class="nav-link">Master Mata Uang</a></li>
          <li class="nav-item"><a href="{{ route('master_barang') }}" class="nav-link">Master Barang</a></li>
          <li class="nav-item"><a href="{{ route('master_gudang') }}" class="nav-link">Master Gudang</a></li>
          <li class="nav-item"><a href="{{ route('master_coa') }}" class="nav-link">Master COA</a></li>
          <li class="nav-item"><a href="{{ route('master_jenis_pos_biaya') }}" class="nav-link">Master Jenis Pos</a></li>
        </ul>
        <a href="Inventory" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
            <span class="menu-item-label">Inventory</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="#" onclick="quality_control()" class="nav-link quality_control">Quality Control</a></li>
          <li class="nav-item"><a href="#" onclick="barang_int()" class="nav-link quality_control">Barang in</a></li>
          <li class="nav-item"><a href="{{ route('barang_out') }}" class="nav-link">Barang Out</a></li>
          <li class="nav-item"><a href="{{ route('pindah_gudang') }}" class="nav-link">Pindah Gudang</a></li>
          <li class="nav-item"><a href="{{ route('ganti_wujud') }}" class="nav-link">Ganti wujud</a></li>
        </ul>
        <a href="/Purchase" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon ion-ios-redo-outline tx-24"></i>
            <span class="menu-item-label">Purchase</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="{{ route('purchase_request') }}" class="nav-link">Purchase Request</a></li>
          <li class="nav-item"><a href="{{ route('purchase_order') }}" class="nav-link">Purchase Order</a></li>
          <li class="nav-item"><a href="{{ route('retur') }}" class="nav-link">Retur</a></li>
        </ul>
        <a href="/Sales" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
            <span class="menu-item-label">Sales</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="{{ route('order_in') }}"{{--  onclick="order_in()" class="nav-link order_in" --}}class="nav-link">Order in</a></li>
          <li class="nav-item"><a href="{{ route('rencana_kirim') }}" class="nav-link">Rencana Kirim</a></li>
          <li class="nav-item"><a href="{{ route('order') }}" class="nav-link">Order</a></li>
          <li class="nav-item"><a href="{{ route('proforma_invoice') }}" class="nav-link">Proforma Invoice</a></li>
          <li class="nav-item"><a href="{{ route('surat_jalan') }}" class="nav-link">Surat Jalan</a></li>
          <li class="nav-item"><a href="{{ route('confirm_sj') }}" class="nav-link">Confirm Sj</a></li>
          <li class="nav-item"><a href="{{ route('reroute_sj') }}" class="nav-link">Re Route Sj</a></li>
          <li class="nav-item"><a href="{{ route('cansel_closing') }}" class="nav-link">Cancel/Closing</a></li>
        </ul>
        <a href="/Shipping" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
            <span class="menu-item-label">Shipping</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="{{ route('order_jadwal_kapan') }}" class="nav-link">Order Jadwal Kapal</a></li>
          <li class="nav-item"><a href="{{ route('konfirmasi_order') }}" class="nav-link">Konirmasi Order</a></li>
          <li class="nav-item"><a href="{{ route('stuffing') }}" class="nav-link">Stuffing</a></li>
          <li class="nav-item"><a href="{{ route('hasil_timbang') }}" class="nav-link">Hasil Timbang</a></li>
          <li class="nav-item"><a href="{{ route('invoice_packlist') }}" class="nav-link">Invoice Pack List</a></li>
          <li class="nav-item"><a href="{{ route('dokumen') }}" class="nav-link">Dokumen</a></li>
          <li class="nav-item"><a href="{{ route('invoice') }}" class="nav-link">Invoice</a></li>
        </ul>
        <a href="/Finance" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-briefcase-outline tx-22"></i>
            <span class="menu-item-label">Finance</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="{{ route('hutang') }}" class="nav-link">Utang</a></li>
          <li class="nav-item"><a href="{{ route('piutang') }}" class="nav-link">Piutang</a></li>
          <li class="nav-item"><a href="{{ route('kas') }}" class="nav-link">Kas</a></li>
          <li class="nav-item"><a href="{{ route('general_ledger') }}" class="nav-link">General Ledger</a></li>
        </ul>
        <a href="/Monitoring" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-briefcase-outline tx-22"></i>
            <span class="menu-item-label">Monitoring</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="{{ route('monitoring_sales') }}" class="nav-link">Monitoring sales</a></li>
          <li class="nav-item"><a href="{{ route('monitoring_purchase') }}" class="nav-link">Monitoring Purchase</a></li>
        </ul>
        <a href="/Report" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-briefcase-outline tx-22"></i>
            <span class="menu-item-label">Report</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="{{ route('report_sales') }}" class="nav-link">Report sales</a></li>
          <li class="nav-item"><a href="{{ route('report_purchase') }}" class="nav-link">Report Purcase</a></li>
        </ul>
      </div><!-- br-sideleft-menu -->

      <label class="sidebar-label pd-x-15 mg-t-25 mg-b-5 tx-info op-9">Information Summary</label>
      <div class="br-sideleft-menu">
        <a href="/Control Panel" class="br-menu-link">
          <div class="br-menu-item">
            <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
            <span class="menu-item-label">Control Panel</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
          <li class="nav-item"><a href="#" class="nav-link">Master Pegawai</a></li>
          <li class="nav-item"><a href="#" class="nav-link">Jabatan</a></li>
          <li class="nav-item"><a href="#" class="nav-link">Permission</a></li>
        </ul>


        </div><!-- d-flex -->
      </div><!-- info-lst -->

      <br>
    </div><!-- br-sideleft -->