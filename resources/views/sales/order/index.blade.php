@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')
      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Index Data</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            {{-- <div class="text-right mg-r-25"> --}}
               {{-- <a href="{{ route('order_in_create') }}" class="btn waves-effect waves-light btn-md btn-success"><i class="fa fa-plus"></i> Add Data</a> --}}
            {{-- </div> --}}
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                  <thead>
                    <tr>
                      <th class="wd-15p">No SP</th>
                      <th class="wd-15p">No Ref</th>
                      <th class="wd-15p">Barang</th>
                      <th class="wd-20p">Qty </th>
                      <th class="wd-15p">Gudang</th>
                      {{-- <th class="wd-10p">Salary</th> --}}
                      <th class="wd-25p">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach ($data as $element)
                    <tr>
                      <td><button value="{{ $element->no_sp_dtl }}" class="btn btn-primary btn-sm create">{{ $element->no_sp_dtl }}</button>
                        {{-- <a href="#" >{{ $element->No_sp }}</a></td> --}}
                      <td>{{ $element->no_sp }}</td>
                      <td>{{ $element->kd_barang }} / {{ $element->Nama_Barang }}</td>
                      <td>{{ $element->jumlah }}</td>
                      <td>{{ $element->kd_gudang }} / {{ $element->Nama_Gudang }}</td>
                      <td>
                        <button class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> </button>  
                        <button class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>  
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>


    @endsection

@section('extra_scripts')

<script>


  $('.create').click(function () {
    // alert($(this).val());
    window.location.assign('order/create/'+$(this).val());
  })


</script>
    @endsection

       