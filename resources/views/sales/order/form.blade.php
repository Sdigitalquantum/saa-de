@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')
      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Form Data</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
            <form class="save">
              <div class="form-layout form-layout-1">
                  <div class="row ">
                      <label class="col-sm-2 form-control-label">Customer</label>
                      <div class="col-sm-4 mg-t-5 mg-sm-t-0">
                        <select class="form-control" name="customer">
                          <option selected="">- Chose -</option>
                          @foreach ($customer as $element)
                            @if ($element->Kd_Customer == $data->Kd_Customer)
                              <option selected="" value="{{ $element->Kd_Customer }}">{{ $element->Kd_Customer }} - {{ $element->Nama_Customer }}</option>
                            @else
                              <option value="{{ $element->Kd_Customer }}">{{ $element->Kd_Customer }} - {{ $element->Nama_Customer }}</option>
                            @endif
                          @endforeach
                      </select>
                      </div>

                      <label class="col-sm-2 form-control-label">Atas nama</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="atas_nama" value="{{ $data->Atas_Nama }}" placeholder="Enter lastname">
                      </div>  
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>

                      <label class="col-sm-2 form-control-label">Sales</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                         <select class="form-control" name="sales">
                          <option selected="">- Chose -</option>
                          @foreach ($sales as $element)
                            @if ($element->Kode_Sales == $data->Kd_sales)
                              <option selected="" value="{{ $element->Kode_Sales }}">{{ $element->Kode_Sales }} - {{ $element->Nama_Sales }}</option>
                            @else
                              <option value="{{ $element->Kode_Sales }}">{{ $element->Kode_Sales }} - {{ $element->Nama_Sales }}</option>
                            @endif
                          @endforeach
                      </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Jenis Order</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                       <select class="form-control" name="jenis_order">
                          @if ($data->Jenis_sp == 'biasa')
                            <option selected="" value="biasa">Biasa</option>
                            <option value="penting">Penting</option>
                          @else
                            <option value="biasa">Biasa</option>
                            <option selected="" value="penting">Penting</option>
                          @endif
                         </select>
                      </div>
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <label class="col-sm-2 form-control-label">Code</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="code" value="{{ $data->no_sp }}"  placeholder="Enter address">
                      </div>
                      <label class="col-sm-2 form-control-label">Tanggal Po</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="date" name="tgl_po" value="{{ $data->Tgl_sp }}"  placeholder="Enter address">
                      </div>
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                    <label class="col-sm-2 form-control-label">Spp ref</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="spp_ref" value="{{ $data->SP_REFF }}" placeholder="Enter address">
                      </div>
                      <label class="col-sm-2 form-control-label">Tanggal Kirim</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="date" name="tgl_kirim" value="{{ $data->Tgl_Kirim }}"  placeholder="Enter address">
                      </div>
                      <label class="col-sm-2 form-control-label">PPN</label>
                      <div class="col-sm-2 mg-b-5 mg-sm-t-0">
                          <label class="rdiobox">
                          @if ($data->Flag_Ppn == 'Y')
                          <input name="ppn" checked="" type="radio">
                          @else
                          <input name="ppn" type="radio">
                          @endif
                          <span>Y</span>
                          </label>
                      </div>
                      <div class="col-sm-2 mg-b-5 mg-sm-t-0">
                          <label class="rdiobox">
                          @if ($data->Flag_Ppn == 'N')
                          <input name="ppn" checked="" type="radio">
                          @else
                          <input name="ppn" type="radio">
                          @endif
                          <span>N</span>
                        </label>
                      </div>
                      <hr>
                      <hr>
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <label class="col-sm-2 form-control-label">Tgl Kirim Marketing</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="date" name="tg_kirim_market" >
                      </div>
                      <label class="col-sm-2 form-control-label">Kota</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="kota_id">
                      </div>
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <label class="col-sm-2 form-control-label">No Tlp</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="tlp" >
                      </div>
                      <label class="col-sm-2 form-control-label">Kode Wilayah</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="wilayah_id">
                      </div>
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>

                    <div><a class="btn btn-info btn-icon rounded-circle mg-r-5 mg-b-10 save" onclick="save()"><div><i style="color: white" class="fa fa-save"></i></div></a></div>


                  </div><!-- row -->

              

                    {{-- <button class="btn btn-primary btnAdd btn-sm" type="button"><i class="fa fa-table"> </i> Add new</button> --}}
                    {{-- <a class="btn btn-success btn-icon rounded-circle mg-b-10 btnAdd"><div><i style="color: white" class="fa fa-plus"></i></div> --}}
                    {{-- </a> --}}
                    {{-- </a> --}}
                    {{-- <button class="btn btn-success save btn-sm text-right" type="button"  onclick="save()"><i class="fa fa-table"> </i> Save</button> --}}
                    <table id="s" class="table display responsive nowrap">
                        <thead>
                          <tr>
                            <th class="wd-15p">No SP</th>
                            <th class="wd-15p">No Ref</th>
                            <th class="wd-15p">Barang</th>
                            <th class="wd-20p">Qty </th>
                            <th class="wd-15p">Gudang</th>
                            {{-- <th class="wd-10p">Salary</th> --}}
                            {{-- <th class="wd-25p">Action</th> --}}
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($datadetil as $el)
                          <tr>
                            <td><input type="hidden" name="no_sp_dtl[]">{{ $el->no_sp_dtl }}</td>
                              {{-- <a href="#" >{{ $el->No_sp }}</a></td> --}}
                            <td><input type="hidden" name="no_sp[]">{{ $el->no_sp }}</td>
                            <td><input type="hidden" name="kd_barang[]">{{ $el->kd_barang }} / {{ $el->Nama_Barang }}</td>
                            <td><input type="hidden" name="jumlah[]">{{ $el->jumlah }}</td>
                            <td><input type="hidden" name="kd_gudang[]">{{ $el->kd_gudang }} / {{ $el->Nama_Gudang }}</td>
                            {{-- <td> --}}
                              {{-- <button class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> </button>   --}}
                              {{-- <button class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>   --}}
                            {{-- </td> --}}
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                     </form>
             </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>


    @endsection

@section('extra_scripts')

<script>
      var table           = $("#s").DataTable({
    });
     function save() {

      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "get",
            url:'{{ route('order_save') }}',
            data: $('.save').serialize(),
            processData: false,
            contentType: false,
          success:function(data){
            if (data.status == 'sukses') {
               
              alert('sukses');
                location.reload();
            }else if (data.status == 'ada') {
              alert('sukses');
                
            }
          },error:function(){
              alert('error');
            
            
          }
        });
        
    }
            
                                   
        
</script>
    @endsection

       