@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')
      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Form Data</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            <br>	
            
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
			            	<form class="save">
        		          <div class="form-layout form-layout-1">

        			            <div class="table-wrapper">
        			            		<table class="table display responsive nowrap">
        			            		<tr>	
        			            			<th class="wd-15p">No Order</th>
        			            			<th class="wd-15p">Kode Barang</th>
        			            			<th class="wd-15p">Nama Barang</th>
        			            			<th >QTY</th>
        			            			<th >Stock</th>
        			            			<th >Available</th>
        			            			<th >Kirim</th>
        			            			<th >Gudang</th>
        			            			<th >Beli</th>
        			            			<th class="wd-15p">Batch</th>
        			            			<th class="wd-15p">Action</th>
        			            		</tr>
        		                   		@foreach ($datadetil as $element)
            			            		<tr class="saving">
            			            			<td><input type="hidden" name="no_sp" class="no_sp_id" value="{{ $element->No_sp }}">{{ $element->No_sp }}</td>	
            			            			<td class="kode_barang">{{ $element->Kd_Stok }}</td>	
            			            			<td><button class="btn btn-sm btn-primary id_brg" type="button" value="{{ $element->Kd_Stok }}" onclick="cekgudang()">{{ $element->Nama_Barang }}</button></td>	
            			            			<td>{{ floatval($element->Qty) }}</td>	
            			            			<td>{{ floatval($element->akhir_qty) }}</td>	
            			            			<td>{{ floatval($element->akhir_qty) }}</td>	
            			            			<td class="change_qty_kirim">
            			            				<input type="text" class="qty_kirim" name="qty_kirim[]" style="width:50px !important"  readonly="">
            			            			</td>
            			            			<td class="change_gudang">
            			            				<input type="text" class="gudang" name="gudang[]" style="width:50px !important" readonly="">
            			            			</td>	
            			            			<td class="change_qty_beli">
            			            				{{-- <input type="text" class="qty_beli" name="qty_beli[]"> --}}
            			            			</td>	
            			            			<td class="change_batch">
            			            				<input type="text" class="batch" name="batch[]" readonly="">
            			            			</td>	
            			            			<td class="drop_here_btn">{{-- {{ $element->No_sp }} --}}</td>	
            			            		</tr>	
        			            		     @endforeach
        			            		</table>
        			            </div>
                    </div>
              </form>
		                {{-- <button class="btn btn-primary btnAdd btn-sm" type="button"><i class="fa fa-table"> </i> Add new</button> --}}
		                {{-- <button class="btn btn-success save btn-sm text-right" type="button"  onclick="save()"><i class="fa fa-table"> </i> Save</button> --}}
		               
                      <div class="table-wrapper">
                        <table id="t72a" class="table display responsive nowrap">
  		                   <thead>
  		                     <tr>
  		                       <th width="10%">Item Name</th>
  		                       <th>Gudang </th>
  		                       <th>Stock</th>
  		                       <th>Available</th>
  		                       <th>Indent</th>
  		                       <th>Kirim</th>
  		                       <th>No Order</th>
  		                       <th>Action</th>
  		                     </tr>
  		                   </thead>
  		                   <tbody class="drop_here">
  		                   </tbody>
  		                 </table>
                     </div>
		         </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>


    @endsection

@section('extra_scripts')

<script>
	  var table = $("#t72a").DataTable({
      // "scrollX": true,
      responsive: true,
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });
 

    function save() {

      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "get",
            url:'{{ route('order_in_save') }}',
            data: $('.save').serialize(),
            processData: false,
            contentType: false,
          success:function(data){
            if (data.status == 'sukses') {
               
            	alert('sukses');
                location.reload();
            }else if (data.status == 'ada') {
            	alert('sukses');
                
            }
          },error:function(){
            	alert('error');
            
          }
        });
        
    }
    function convertDecimalToRupiah(decimal) 
  	{
 	   var angka = parseFloat(decimal);	
       return angka;
	}

    function cekgudang(ar){

    	var id = $('.id_brg').val();
    	var ns = $('.no_sp_id').val();

    	$.ajax({
            type: "get",
            url:'{{ route('rencana_kirim_check_gudang') }}',
            data: {'id':id,'ns':ns},
            success:function(data){
            $('.drop_here').html(data);


         //   	key = 0;

         //   	Object.keys(data.gudang).forEach(function(){
         //   	// $.each(data.gudang, function (index, value) {
	        //   $('.drop_here').html(
	        //     "<tr clas='gg'>"+
	        //     '<td style="text-align:center"><input type="hidden" name="item[]" value="'+data.gudang[key].kd_stok+'" class="form-control">'+data.gudang[key].kd_stok+'</td>'+
	        //     '<td style="text-align:center"><input type="hidden" name="confirmqty[]" value="'+data.gudang[key].kode_gudang+'" class="form-control gudang_ut">'+data.gudang[key].kode_gudang+'</td>'+
	        //     '<td style="text-align:center"><input type="hidden" name="confirmqty[]" value="'+convertDecimalToRupiah(data.gudang[key].qty_in)+'" class="form-control ">'+convertDecimalToRupiah(data.gudang[key].qty_in)+'</td>'+
	        //     '<td style="text-align:center"><input type="hidden" name="confirmqty[]" value="'+convertDecimalToRupiah(data.gudang[key].akhir_qty)+'" class="form-control">'+convertDecimalToRupiah(data.gudang[key].akhir_qty)+'</td>'+
	        //     '<td style="text-align:center"><input type="hidden" name="confirmqty[]" value="'+convertDecimalToRupiah(data.gudang[key].qty_in)+'" class="form-control">'+convertDecimalToRupiah(data.gudang[key].qty_in)+'</td>'+
	        //     '<td style="text-align:center"><input type="text" name="kirim[]" class="form-control kirim"></td>'+
	        //     '<td style="text-align:center"><input type="hidden" name="confirmqty[]" value="'+(data.no_sp.No_sp+"-"+[key+1])+'" class="form-control">'+(data.no_sp.No_sp+"-"+[key+1])+'</td>'+
	        //     '<td style="text-align:center"><input type="hidden" name="branch[]" value="'+(data.no_sp.No_sp+"-"+[key+1])+'" class="form-control branch">'+
         //      '<button  class="btn btn-sm btn-success btn_save_up"  type="button"  data-id="'+(data.gudang[key].kd_stok)+'" value="'+(data.gudang[key].kd_stok)+'">Save</button>'+
         //      '</td>'+
	        //     // '<td style="text-align:center"></td>'+
	        //     "</tr>"
	        //   );

	        // key++;
	        // });
    	    //  $(this).find('.id_brg').attr('disabled',true);

         //  },error:function(){
         //    	alert('error');
            
         //  }



        }
    });  
    }    
    $('#t72a tbody').on( 'click', '.btn_save_up', function () {
        // var parent = $(this).parents('tr');
        // var get = $(parent).
        var parents = $(this).parents('tr');
      	var kode = $(parents).find('.btn_save_up').val();
      	var kirim = $(parents).find('.kirim').val();
      	var branch = $(parents).find('.no_sp').val();
      	var gudang = $(parents).find('.gudang_ut').val();
      	console.log(kode);
      	console.log(kirim);
      	console.log(branch);
      	console.log(gudang);
      	// var change_qty_kirim = $('.change_qty_kirim').text(kirim);
      	// var change_qty_gudang = $('.change_gudang').text(gudang);
      	// var change_qty_batch = $('.change_batch').text(branch);

        var qty_kirim = $('.qty_kirim').val(kirim);
        var gudang = $('.gudang').val(gudang);
        var batch = $('.batch').val(branch);
      	// console.log(get_pasang);
      	var simpan = $('.drop_here_btn').html(
      		// '<input type="button" class="sss"  value="'+branch+'"></input>'
          '<button type="button" value"'+branch+'" class="save_data btn btn-success"><i class="fa fa-save"></i> Save</button>'
      	);
    });

   
    $(document).on('click', '.sss',function(){
      $check = event.target.value;
      // var parents = $(this).parents('tr');
      var qty_kirim = $(this).parents('.saving').find('.qty_kirim').val();
      var gudang = $(this).parents('.saving').find('.gudang').val();
      var batch = $(this).parents('.saving').find('.batch').val();
      // alert($check);
      // alert(argument);
      console.log(event.target.value);
      console.log(qty_kirim);
      console.log(gudang);
      console.log(batch);
      // $('.saving').
    	
    // }
    })

    $(document).on('click', '.save_data',function (argument) {
      var qty_kirim = $(this).parents('.saving').find('.qty_kirim').val();
      var gudang = $(this).parents('.saving').find('.gudang').val();
      var batch = $(this).parents('.saving').find('.batch').val();
      var no_sp_id = $(this).parents('.saving').find('.no_sp_id').val();
      var kode_barang = $(this).parents('.saving').find('.kode_barang').text();
      console.log(qty_kirim);
      console.log(gudang);
      console.log(batch);

      $.ajax({
        type: "get",
        url:'{{ route('rencana_kirim_save') }}',
        data: {'qty_kirim':qty_kirim,'gudang':gudang,'batch':batch,'no_sp_id':no_sp_id,'kode_barang':kode_barang},
        success:function(data){
        }
      })

    });
    // onclick="save_top('+data.gudang[key].kd_stok+')"
    // function save_top(argument) {

    	// alert($(this).data('id'));
    	// alert($(this).val());
    // }            

    // $('.btn_save_up').click(function(){
    	// alert('a');
    // })                 
        
</script>
    @endsection

       