@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')
      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Index Data</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            <br>
            <div class="row col-md-12" >
            	<div class="col-md-10">
            		<table class="table table-stripped" width="100%">
            			<tr>
            				<th>Date First </th>
            				<th>:</th>
            				<th><input class="form-control datepicker fc-datepicker" type="text" name="firstname" value="dd/mm/yyyy" name="date_first" id="date_first" placeholder=""></th>
            				<th>&nbsp;</th>
            				<th>Date End </th>
            				<th>:</th>
            				<th><input class="form-control datepicker fc-datepicker" type="text" name="firstname" value="dd/mm/yyyy" name="date_end" id="date_end" placeholder=""></th>
            			</tr>
            		</table>
            	</div>
	            <div class="col-md-2s">
            		 <button class="btn btn-teal btn-sm"><i class="fa fa-search"></i> Search</button>
	            	 
	            	 <a href="{{ route('order_in_create') }}" class="btn waves-effect waves-light btn-sm btn-info"><i class="fa fa-plus"></i> Add Data</a>
	            </div>
            </div>
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
		            <table id="datatable1" class="table display responsive nowrap">
		              <thead>
		                <tr>
		                  <th class="wd-15p">Customer</th>
		                  <th class="wd-15p">Atas nama</th>
		                  <th class="wd-20p">Sales</th>
		                  <th class="wd-15p">Jenis Order</th>
		                  <th class="wd-25p">Action</th>
		                </tr>
		              </thead>
		              <tbody>
		                {{-- <tr>
		                  <td>Tiger</td>
		                  <td>Nixon</td>
		                  <td>System Architect</td>
		                  <td>2011/04/25</td>
		                  <td>
		                  	<a href="#" class="btn btn-warning btn-icon rounded-circle"><div><i class="fa fa-pencil"></i></div></a>
		                  	<a href="#" class="btn btn-danger btn-icon rounded-circle"><div><i class="fa fa-times"></i></div></a>
                      	  </td>
                  		</tr> --}}
		              </tbody>
		            </table>
		          </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>


    @endsection

@section('extra_scripts')

<script>

	function search() {
		$first = $('#date_first').val();
		$end = ('#date_end').val();

      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "get",
            url:'{{ route('quotation_search') }}',
            data: {'first':$first,'end':$end},
            processData: false,
            contentType: false,
          success:function(data){
            if (data.status == 'sukses') {
            	alert('sukses');
                location.reload();
            }else if (data.status == 'ada') {
            	alert('data sudah ada');
            }
          },error:function(){
            	alert('error');
            
            
          }
        });
        
    }




</script>
@endsection

       