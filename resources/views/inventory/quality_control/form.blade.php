@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')

      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Form Data</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
            <form class="save">
              <div class="form-layout form-layout-1">
                  <div class="row ">
                    <label class="col-sm-2 form-control-label">No Purchase</label>
                      <div class="col-sm-10 mg-t-5 mg-sm-t-0">
                        <select class="form-control" name="no_pr" id="no_pr">
                          <option selected="">- Chose -</option>
                          @foreach ($po as $element)
                            <option value="{{ $element->no_po }}">{{ $element->no_po }}</option>
                          @endforeach
                      </select>
                      </div>
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                    <label class="col-sm-2 form-control-label">No Order</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="code" value=""  disabled="" placeholder="Auto Gen">
                      </div>
                      <label class="col-sm-2 form-control-label">Supplier</label>
                      <div class="col-sm-4 mg-t-5 mg-sm-t-0">
                        {{-- <select class="form-control" name="customer" id="customer">
                          <option selected="">- Chose -</option>
                          @foreach ($customer as $element)
                            <option value="{{ $element->Kd_Customer }}" data-name="{{ $element->nama_customer2 }}" data-alamat="{{ $element->Alamat1 }}">{{ $element->Kd_Customer }} - {{ $element->Nama_Customer }}</option>
                          @endforeach
                      </select> --}}
                        <input class="form-control" type="hidden" name="supplier" id="supplier" >
                        <input class="form-control" type="text" id="sup1">

                      </div>

                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <label class="col-sm-2 form-control-label">Alamat</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control alamat" type="text" name="alamat" id="alamat" placeholder="Enter lastname">
                      </div>  
                     {{--  <label class="col-sm-2 form-control-label">Atas nama</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control atas_nama" type="text" name="atas_nama" id="atas_nama" placeholder="Enter lastname">
                      </div>   --}}
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <label class="col-sm-2 form-control-label">Tanggal Po</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="date" name="tgl_pr" id="tgl_pr"  placeholder="Enter address">
                      </div>
                      <label class="col-sm-2 form-control-label">Gudang</label>
                      <div class="col-sm-4 mg-t-5 mg-sm-t-0">
                       {{--  <select class="form-control" name="gudang" id="gudang">
                          <option selected="">- Chose -</option>
                          @foreach ($gudang as $element)
                            <option value="{{ $element->Kode_Gudang }}">{{ $element->Kode_Gudang }} - {{ $element->Nama_Gudang }}</option>
                          @endforeach
                      </select> --}}
                        <input class="form-control" type="hidden" name="gudang" id="gudang" >
                        <input class="form-control" type="text"  id="gud1">
                      </div>
                      </div>
                  </div><!-- row -->

              

                    {{-- <button class="btn btn-primary btnAdd btn-sm" type="button"><i class="fa fa-table"> </i> Add new</button> --}}
                    {{-- <a class="btn btn-success btn-icon rounded-circle mg-b-10 btnAdd"><div><i style="color: white" class="fa fa-plus"></i></div>
                    </a> --}}
                    <a class="btn btn-info btn-icon rounded-circle mg-r-5 mg-b-10 save" onclick="save()"><div><i style="color: white" class="fa fa-save"></i></div>
                    </a>
                    {{-- <button class="btn btn-success save btn-sm text-right" type="button"  onclick="save()"><i class="fa fa-table"> </i> Save</button> --}}
                         <table id="t72a">
                       <thead>
                         <tr>
                           <th width="20%">Item Name</th>
                           <th width="30%">Deskipsi</th>
                           <th>Qty </th>
                           <th>Qty Confirm </th>
                           <th width="10%">satuan</th>
                           <th width="20%">Harga</th>
                         </tr>
                       </thead>
                       <tbody class="drop_here">
                       </tbody>
                     </table>
                     </form>
             </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>
        {{-- @include('purchase.purchase_order.ajax_detail_order') --}}

    @endsection

@section('extra_scripts')

<script>


    var table           = $("#t72a").DataTable({
    });

     function save() {
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "get",
            url:'{{ route('quality_control_save') }}',
            data: $('.save').serialize(),
            processData: false,
            contentType: false,
          success:function(data){
            if (data.status == 'sukses') {
               
              alert('sukses');
                location.reload();
            }else if (data.status == 'ada') {
              alert('sukses');
                
            }
          },error:function(){
              alert('error');
            
            
          }
        });
        
    }
     

      $('#no_pr').change(function(){
          var id = $(this).val();
          $.ajax({
            type: "get",
            url:baseUrl+'/purchase/purchase_order/cari/'+id,
            datatype:"JSON",
            processData: false,
            contentType: false,
            success:function(data){
              console.log(data);
              $('#supplier').val(data.data.Kode_Supplier);
              $('#sup1').val(data.data.Nama_Supplier);
              $('#gud1').val(data.data.Nama_Gudang);
              $('#alamat').val(data.data.alamat);
              $('#atas_nama').val(data.data.atas_nama);
              $('#tgl_pr').val(data.data.tgl_pr);
              $('#gudang').val(data.data.kd_gudang);


              // key = 0;
                $('.drop_here').empty();

              // Object.keys(data.detil).forEach(function(){
               $.each(data.detil, function (index, value) {
                
                $('.drop_here').append(
                  "<tr clas='gg'>"+
                    '<td style="text-align:center"><input type="hidden" name="kd_stok[]" value="'+data.detil[index].kd_stok+'" class="form-control">'+data.detil[index].kd_stok+'</td>'+
                    '<td style="text-align:center"><input type="hidden" name="keterangan[]" value="'+data.detil[index].keterangan+'" class="form-control detil_ut">'+data.detil[index].keterangan+'</td>'+
                    '<td style="text-align:center"><input type="hidden" name="qty[]" value="'+data.detil[index].qty+'" class="form-control">'+data.detil[index].qty+'</td>'+
                    '<td style="text-align:center"><input type="text" name="qty_confirm[]"></td>'+
                    '<td style="text-align:center"><input type="hidden" name="kd_satuan[]" value="'+data.detil[index].kd_satuan+'" class="form-control">'+data.detil[index].kd_satuan+'</td>'+
                    '<td style="text-align:center"><input type="hidden" name="harga[]" value="'+data.detil[index].harga+'" class="form-control">'+data.detil[index].harga+'</td>'+
                    '</td>'+
                  // '<td style="text-align:center"></td>'+
                  "</tr>"
                );



              // key++;
            });

            },error:function(){
                alert('error');
            }
        });

     })
       
       $(document).on('change','.item_name',function(){

          // var parents = $(this).parents('tr');
          var item_kode = $(this).parents('.parent_tr').find('.item_name :selected').data('desc');
          console.log(item_kode);
          var ss = $(this).parents('.parent_tr').find('.deskipsi').val(item_kode);

       })                
        
</script>
    @endsection

       