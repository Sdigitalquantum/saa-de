@extends('layouts_backend._main_backend')

@section('extra_styles')
@endsection

@section('content')
      <div class="col-md-12 mg-t-30 mg-md-t-45">
          <div class="card bd-0">
            <div class="card-header bg-dark bd-0 d-flex align-items-center justify-content-between pd-y-5">
              <h6 class="mg-b-0 tx-14 tx-white tx-normal">Form Data</h6>
              <div class="card-option tx-24">
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-refresh-empty lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-ios-arrow-down lh-0"></i></a>
                <a href="" class="tx-gray-600 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
              </div><!-- card-option -->
            </div><!-- card-header -->
            
            <div class="card-body bd bd-t-0 rounded-bottom">
                <div class="table-wrapper">
            <form class="save">
              <div class="form-layout form-layout-1">
                  <div class="row ">
                      
                      <label class="col-sm-2 form-control-label">No Purchase</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="text" name="code" value=""  placeholder="Auto Gen">
                      </div>
                      <label class="col-sm-2 form-control-label">Supplier</label>
                      <div class="col-sm-4 mg-t-5 mg-sm-t-0">
                        <select class="form-control" name="supplier" id="supplier">
                          <option selected="">- Chose -</option>
                          @foreach ($supplier as $element)
                            <option value="{{ $element->Kode_Supplier }}"  data-alamat="{{ $element->Alamat1 }}">{{ $element->Kode_Supplier }} - {{ $element->Nama_Supplier }}</option>
                          @endforeach
                      </select>
                      </div>

                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <label class="col-sm-2 form-control-label">Alamat</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control alamat" type="text" name="alamat" placeholder="Enter lastname">
                      </div>  
                     {{--  <label class="col-sm-2 form-control-label">Atas nama</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control atas_nama" type="text" name="atas_nama" placeholder="Enter lastname">
                      </div>   --}}
                      <div class="col-sm-12 mg-b-5 mg-sm-t-0"></div>
                      <label class="col-sm-2 form-control-label">Tanggal Po</label>
                      <div class="col-sm-4 mg-b-5 mg-sm-t-0">
                        <input class="form-control" type="date" name="tgl_pr"  placeholder="Enter address">
                      </div>
                      <label class="col-sm-2 form-control-label">Gudang</label>
                      <div class="col-sm-4 mg-t-5 mg-sm-t-0">
                        <select class="form-control" name="gudang" id="gudang">
                          <option selected="">- Chose -</option>
                          @foreach ($gudang as $element)
                            <option value="{{ $element->Kode_Gudang }}">{{ $element->Kode_Gudang }} - {{ $element->Nama_Gudang }}</option>
                          @endforeach
                      </select>
                      </div>
                      </div>
                  </div><!-- row -->

              

                    {{-- <button class="btn btn-primary btnAdd btn-sm" type="button"><i class="fa fa-table"> </i> Add new</button> --}}
                    <a class="btn btn-success btn-icon rounded-circle mg-b-10 btnAdd"><div><i style="color: white" class="fa fa-plus"></i></div>
                    </a>
                    <a class="btn btn-info btn-icon rounded-circle mg-r-5 mg-b-10 save" onclick="save()"><div><i style="color: white" class="fa fa-save"></i></div>
                    </a>
                    {{-- <button class="btn btn-success save btn-sm text-right" type="button"  onclick="save()"><i class="fa fa-table"> </i> Save</button> --}}
                         <table id="t72a">
                       <thead>
                         <tr>
                           <th width="20%">Item Name</th>
                           <th width="30%">Deskipsi</th>
                           <th>Qty </th>
                           <th width="10%">satuan</th>
                           <th width="20%">Harga</th>
                           <th>Action</th>
                         </tr>
                       </thead>
                       <tbody>
                           <tr class="parent_tr">
                            <td>
                                <select class="form-control form-control-sm min-width item_name readonly" name="ite_name[]">
                            <option selected="">- Chose -</option>
                            @foreach ($barang as $element)
                              <option value="{{ $element->Kode_Barang }}" data-desc="{{ $element->Nama_Barang }}">{{ $element->Kode_Barang }} - {{ $element->Nama_Barang }}</option>
                            @endforeach
                          </select>
                        {{-- <input type="text" class="form-control form-control-sm min-width readonly" name="ite_name[]"></td> --}}
                          </td>
                              <td><input type="text" class="form-control form-control deskipsi sm min-width readonly" name="deskipsi[]"></td>
                              
                              <td><input type="text" class="form-control form-control-sm min-width2 readonly" name="qty[]" ></td>
                              <td>
                                <select class="form-control form-control-sm min-width readonly" name="satuan[]">
                            <option selected="" value="KG">KG</option>
                            <option selected="" value="BAG">BAG</option>
                            <option selected="" value="BUTIR">BUTIR</option>
                          </select>
                              </td>
                              <td><input type="text" class="form-control form-control-sm min-width right format_money total_price readonly" name="harga[]" ></td>
                              <td><button type="button" class="delete btn btn-outline-danger btn-sm hapus"><i class="fa fa-trash"></i></button></td>
                           </tr>
                       </tbody>
                     </table>
                     </form>
             </div><!-- table-wrapper -->
            </div><!-- card-body -->
          </div><!-- card -->
        </div>


    @endsection

@section('extra_scripts')

<script>
    var table           = $("#t72a").DataTable({
    });
    var rp_qty         = $(".btnAdd");
    var rp_item          = $("#rp_item");
    var rp_kodeitem       = $("#rp_kodeitem");

    var x = 1;

    // $(document).ready (function () {                
         rp_qty.click(function(e) {

        // $(row).addClass("label-warning");

        var row = table.row.add( [
          '<select class="form-control form-control-sm item_name min-width readonly" name="ite_name[]"><option selected="">- Chose -</option>@foreach ($barang as $element)<option value="{{ $element->Kode_Barang }}" data-desc="{{ $element->Nama_Barang }}">{{ $element->Kode_Barang }} - {{ $element->Nama_Barang }}</option>@endforeach</select>',
            '<input type="text" class="form-control form-control-sm min-width deskipsi readonly" name="deskipsi[]">',
            
            '<input type="text" class="form-control form-control-sm min-width2 readonly" name="qty[]" >',
            '<select class="form-control form-control-sm min-width readonly" name="satuan[]"><option selected="" value="KG">KG</option>'+'<option selected="" value="BAG">BAG</option>'+'<option selected="" value="BUTIR">BUTIR</option>'+
            '</select>',
            '<input type="text" class="form-control form-control-sm min-width right format_money total_price readonly" name="harga[]" >',
            '<button type="button" class="delete btn btn-outline-danger btn-sm hapus"><i class="fa fa-trash"></i></button>',
        ] ).node();

    $(row).addClass( 'parent_tr' );
        table.draw( false );
        x++;


    });



    $('#t72a tbody').on( 'click', '.delete', function () {
        table.row($(this).parents('tr'))
            .remove()
            .draw();

    });         

     function save() {

      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "get",
            url:'{{ route('purchase_request_save') }}',
            data: $('.save').serialize(),
            processData: false,
            contentType: false,
          success:function(data){
            if (data.status == 'sukses') {
               
              alert('sukses');
                location.reload();
            }else if (data.status == 'ada') {
              alert('sukses');
                
            }
          },error:function(){
              alert('error');
            
            
          }
        });
        
    }
     
     $('#supplier').change(function(){
        // alert($(this).val());
        // alert($(this).val());
        var name = $(this).find(':selected').data('name');
        var alamat = $(this).find(':selected').data('alamat');
        console.log(name);
        console.log(alamat);
        $('.alamat').val(alamat);
        $('.atas_nama').val(name);

     })
       
       $(document).on('change','.item_name',function(){

          // var parents = $(this).parents('tr');
          var item_kode = $(this).parents('.parent_tr').find('.item_name :selected').data('desc');
          console.log(item_kode);
          var ss = $(this).parents('.parent_tr').find('.deskipsi').val(item_kode);

       })                
        
</script>
    @endsection

       