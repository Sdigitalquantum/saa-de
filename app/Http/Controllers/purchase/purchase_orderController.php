<?php

namespace App\Http\Controllers\purchase;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\d_site;

class purchase_orderController extends Controller
{

    public function index()
    {

    	$data = DB::table('PURC_PO')->get();

        return view('purchase.purchase_order.index',compact('data'));
    }
    public function create()
    {   
        $barang = DB::table('SIF_Barang')->get();
        $customer = DB::table('SIF_Customer')->get();
        $gudang = DB::table('SIF_Gudang')->get();
        $pr = DB::table('PURC_PR_D')->where('status','FN_PR')->get();


        return view('purchase.purchase_order.form',compact('gudang','barang','customer','pr'));
    }
    public function cari($id)
    {
        // return $id;
        $data = DB::table('PURC_PR_D')->join('SIF_Supplier','SIF_Supplier.Kode_Supplier','=','PURC_PR_D.kd_supplier')->join('SIF_Gudang','SIF_Gudang.Kode_Gudang','=','PURC_PR_D.kd_gudang')->where('no_pr',$id)->first();
        $detil = DB::table('PURC_PR_D_BARANG')->where('no_pr',$id)->get();
        // return jsn
        return response()->json(['data'=>$data,'detil'=>$detil]);
    }
    public function save(Request $request)
    {

    	//get all name/value
        // return $input = $request->all();
        $code = DB::table('PURC_PO')->get();
        // return count($code);
        $itung = count($code);
        if ($itung == null) {
            $itung = 1;
        }
        else{
            $itung +=1;   
        }
        $nota = 'PR-'.date('ym').$itung;
    	//save data
        $data = DB::Table('PURC_PO')->insert([
            'no_po'=>$nota,
            'kd_supplier'=>$request->supplier,
            'kd_gudang'=>$request->gudang,
            'alamat'=>$request->alamat,
            'tgl_po'=>$request->tgl_pr,
        ]);

        for ($i=0; $i <count($request->kd_stok) ; $i++) { 
            $data1 = DB::table('PURC_PO_D')->insert([
                'no_po'=>$nota,
                'kd_stok'=>$request->kd_stok[$i],
                'no_seq'=>$i+1,
                'keterangan'=>$request->keterangan[$i],
                'qty'=>$request->qty[$i],
                'kd_satuan'=>$request->kd_satuan[$i],
                'harga'=>$request->harga[$i],
            ]);
        }

        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
    	$data = DB::table('d_site')->where('s_id',$id)->first();

        return view('master.master_site.edit',compact('data'));
    }
    public function update(Request $request)
    {
    	//get all name/value
        $input = $request->except('s_id');
    	//check unique row , if exist == 1
    	// $check = DB::table('d_site')->where('r_level',$request->r_level)->count();
    	$check = DB::table('d_site')
                        ->where('s_id',$request->s_id)
                        ->first();

        if ($check != null) {
            if ($check->s_id != $request->s_id) {
                return response()->json(['status'=>'ada']);
            }
        }
    	//save data
        $data = d_site::where('s_id', $request->s_id)->update($input);
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
    	$check = DB::table('d_site')->where('s_id',$id)->delete();

    	if ($check == true) {
    		return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);	
    	}
    }
}
