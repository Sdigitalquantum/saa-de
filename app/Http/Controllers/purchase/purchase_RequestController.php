<?php

namespace App\Http\Controllers\purchase;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\d_site;

class purchase_requestController extends Controller
{

    public function index()
    {

    	$data = DB::table('PURC_PR_D')->get();

        return view('purchase.purchase_request.index',compact('data'));
    }
    public function create()
    {   
        $barang = DB::table('SIF_Barang')->get();
        $supplier = DB::table('SIF_Supplier')->get();
        $sales = DB::table('SIF_Sales')->get();
        $gudang = DB::table('SIF_Gudang')->get();

        return view('purchase.purchase_request.form',compact('gudang','barang','supplier','sales'));
    }
    public function save(Request $request)
    {

    	//get all name/value
        $input = $request->all();
        $code = DB::table('PURC_PR_D')->get();
        // return count($code);
        $itung = count($code);
        if ($itung == null) {
            $itung = 1;
        }
        else{
            $itung +=1;   
        }
        $nota = 'PR-'.date('ym').$itung;
    	//save data
        $data = DB::Table('PURC_PR_D')->insert([
            'no_pr'=>$nota,
            'kd_supplier'=>$request->supplier,
            'kd_gudang'=>$request->gudang,
            'alamat'=>$request->alamat,
            'tgl_pr'=>$request->tgl_pr,
            'status'=>'WT',
        ]);
        // return $data;

        for ($i=0; $i <count($request->ite_name) ; $i++) { 
            $data1 = DB::table('PURC_PR_D_BARANG')->insert([
                'no_pr'=>$nota,
                'kd_stok'=>$request->ite_name[$i],
                'no_urut'=>$i+1,
                'keterangan'=>$request->deskipsi[$i],
                'qty'=>$request->qty[$i],
                'kd_satuan'=>$request->satuan[$i],
                'harga'=>$request->harga[$i],
            ]);
        }

        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
    	$data = DB::table('d_site')->where('s_id',$id)->first();

        return view('master.master_site.edit',compact('data'));
    }
    public function update(Request $request)
    {
    	//get all name/value
        $input = $request->except('s_id');
    	//check unique row , if exist == 1
    	// $check = DB::table('d_site')->where('r_level',$request->r_level)->count();
    	$check = DB::table('d_site')
                        ->where('s_id',$request->s_id)
                        ->first();

        if ($check != null) {
            if ($check->s_id != $request->s_id) {
                return response()->json(['status'=>'ada']);
            }
        }
    	//save data
        $data = d_site::where('s_id', $request->s_id)->update($input);
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
    	$check = DB::table('d_site')->where('s_id',$id)->delete();

    	if ($check == true) {
    		return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);	
    	}
    }
    public function index_acc()
    {
        $data = DB::table('PURC_PR_D')->where('status','WT')->get();

        return view('purchase.purchase_request_acc.index',compact('data'));
    }
    public function save_acc(Request $request)
    {
        
    }
}
