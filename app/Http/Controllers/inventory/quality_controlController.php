<?php

namespace App\Http\Controllers\inventory;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;

class quality_controlController extends Controller
{

    public function index()
    {

        return view('inventory.quality_control.index');
    }
    public function create()
    {
        $po = DB::table('PURC_PO')->get();
        return view('inventory.quality_control.form',compact('po'));
    }
    public function save(Request $request)
    {

    	//get all name/value
    	$input = $request->all();
    	//save data
        $input = $request->all();
        $code = DB::table('INV_QC_M')->get();
        // return count($code);
        $itung = count($code);
        if ($itung == null) {
            $itung = 1;
        }
        else{
            $itung +=1;   
        }
        $nota = 'QC-'.date('ym').$itung;
        //save data
        $data = DB::Table('INV_QC_M')->insert([
            'no_trans'=>$nota,
            'kd_supplier'=>$request->supplier,
            'kd_gudang'=>$request->gudang,
            // 'alamat'=>$request->alamat,
            'tgl_trans'=>$request->tgl_trans,
        ]);
        // return $data;
       for ($i=0; $i <count($request->kd_stok) ; $i++) { 
            $data1 = DB::table('INV_QC')->insert([
                'no_trans'=>$nota,
                'kd_stok'=>$request->kd_stok[$i],
                'no_seq'=>$i+1,
                'keterangan'=>$request->keterangan[$i],
                'qty_order'=>$request->qty[$i],
                'qty_datang'=>$request->qty_confirm[$i],
                'kd_satuan'=>$request->kd_satuan[$i],
                'harga'=>$request->harga[$i],
            ]);
        }
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
    	$data = DB::table('d_site')->where('s_id',$id)->first();

        return view('master.master_site.edit',compact('data'));
    }
    public function update(Request $request)
    {
    	//get all name/value
        $input = $request->except('s_id');
    	//check unique row , if exist == 1
    	// $check = DB::table('d_site')->where('r_level',$request->r_level)->count();
    	$check = DB::table('d_site')
                        ->where('s_id',$request->s_id)
                        ->first();

        if ($check != null) {
            if ($check->s_id != $request->s_id) {
                return response()->json(['status'=>'ada']);
            }
        }
    	//save data
        $data = d_site::where('s_id', $request->s_id)->update($input);
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
    	$check = DB::table('d_site')->where('s_id',$id)->delete();

    	if ($check == true) {
    		return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);	
    	}
    }
}
