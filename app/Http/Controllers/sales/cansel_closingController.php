<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\d_unit;

class master_unitController extends Controller
{

    public function index()
    {

    	$data = DB::table('d_unit')->get();

        return view('master.master_unit.index',compact('data'));
    }
    public function create()
    {
        return view('master.master_unit.create');
    }
    public function save(Request $request)
    {

    	//get all name/value
    	$input = $request->all();
    	//save data
        $data = d_unit::create($input);
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
    	$data = DB::table('d_unit')->where('u_id',$id)->first();

        return view('master.master_unit.edit',compact('data'));
    }
    public function update(Request $request)
    {
    	//get all name/value
        $input = $request->except('u_id');
    	//check unique row , if exist == 1
    	// $check = DB::table('d_unit')->where('r_level',$request->r_level)->count();
    	// $check = DB::table('d_unit')
     //                    ->where('u_id',$request->u_id)
     //                    ->first();

     //    if ($check != null) {
     //        if ($check->u_id != $request->u_id) {
     //            return response()->json(['status'=>'ada']);
     //        }
     //    }
    	//save data
        $data = d_unit::where('u_id', $request->u_id)->update($input);
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
    	$check = DB::table('d_unit')->where('u_id',$id)->delete();

    	if ($check == true) {
    		return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);	
    	}
    }
}
