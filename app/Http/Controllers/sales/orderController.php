<?php

namespace App\Http\Controllers\sales;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;

class orderController  extends Controller
{

    public function index()
    {
        
        $data = DB::table('PROD_rcn_krm_temp')->select('PROD_rcn_krm_temp.*','SIF_Barang.Nama_Barang','SIF_Gudang.Nama_Gudang')->join('SIF_Barang','SIF_Barang.Kode_Barang','=','PROD_rcn_krm_temp.kd_barang')->join('SIF_Gudang','SIF_Gudang.Kode_Gudang','=','PROD_rcn_krm_temp.kd_gudang')->get();
        return view('sales.order.index',compact('data'));
    }
    public function create($id)
    {
        
        $code = DB::table('SALES_SO')->get();
        // return count($code);
        $itung = count($code);
        if ($itung == null) {
            $itung = 1;
        }
        else{
            $itung +=1;   
        }
        $nota = 'SO-'.date('ym').$itung;
        $barang = DB::table('SIF_Barang')->get();
        $customer = DB::table('SIF_Customer')->get();
        $sales = DB::table('SIF_Sales')->get();
        $data = DB::table('PROD_rcn_krm_temp')->join('SALES_SO','PROD_rcn_krm_temp.no_sp','=','SALES_SO.No_sp')->first();
        $datadetil = DB::table('PROD_rcn_krm_temp')->select('PROD_rcn_krm_temp.*','SIF_Barang.Nama_Barang','SIF_Gudang.Nama_Gudang')->join('SIF_Barang','SIF_Barang.Kode_Barang','=','PROD_rcn_krm_temp.kd_barang')->join('SIF_Gudang','SIF_Gudang.Kode_Gudang','=','PROD_rcn_krm_temp.kd_gudang')->where('PROD_rcn_krm_temp.no_sp_dtl',$id)->get();

        return view('sales.order.form',compact('nota','barang','customer','sales','data','datadetil'));
    }
    public function check_gudang(Request $request)
    {
        $data = DB::table('INV_STOK_GUDANG')->where('kd_stok',$request->id)->get();
        $no_sp = DB::table('SALES_SO')->select('No_sp')->where('No_sp','=',$request->ns)->first();
        // return json_encode($no_sp);
        // return $no_sp;
        // return response()->json(['gudang'=>$data,'no_sp'=>$no_sp]);
        return view('sales.rencana_kirim.ajax_detail_gudang',compact('data','no_sp'));
    }
    public function save(Request $request)
    {
        // dd($request->all());
        if ($request->ppn == 'on') {
            $ppn = 'Y';
        }else{
            $ppn = 'N';
        }
        $data = DB::table('SALES_SO_ORDER')->insert([
            'Kd_Cabang'=>1,
            'Tipe_trans'=>1,
            'No_sp'=>$request->code,
            'Atas_Nama'=>$request->atas_nama,
            'Kd_Customer'=>$request->customer,
            'Kd_sales'=>$request->sales,
            'Tgl_sp'=>$request->tgl_po,
            'Tgl_Kirim'=>$request->tgl_kirim,
            'Jenis_sp'=>$request->jenis_order,
            'SP_REFF'=>$request->spp_ref,
            'Flag_Ppn'=>$ppn,
            'Tgl_Kirim_Marketing'=>$request->tg_kirim_market,
            'kota_id'=>$request->kota_id,
            'tlp'=>$request->tlp,
            'wilayah_id'=>$request->wilayah_id,
        ]);
        for ($i=0; $i <count($request->no_sp_dtl) ; $i++) { 
            $data2[$i] = DB::table('SALES_SO_ORDER_D')->insert([
            'Kd_Cabang'=>1,
            'No_seq'=>$i+1,
            'No_sp'=>$request->code,
            'no_sp_dtl'=>$request->no_sp_dtl[$i],
            'Kd_Stok'=>$request->kd_barang[$i],
            'Qty'=>$request->jumlah[$i],
            'Last_create_date'=>date('Y-m-d'),
            'kd_gudang'=>$request->kd_gudang[$i],
            ]);
        }

        if ($data == true) {
          return response()->json(['status'=>'sukses']);
        }else{
          return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
      $data = DB::table('d_site')->where('s_id',$id)->first();

        return view('master.master_site.edit',compact('data'));
    }
    public function update(Request $request)
    {
      //get all name/value
        $input = $request->except('s_id');
      //check unique row , if exist == 1
      // $check = DB::table('d_site')->where('r_level',$request->r_level)->count();
      $check = DB::table('d_site')
                        ->where('s_id',$request->s_id)
                        ->first();

        if ($check != null) {
            if ($check->s_id != $request->s_id) {
                return response()->json(['status'=>'ada']);
            }
        }
      //save data
        $data = d_site::where('s_id', $request->s_id)->update($input);
        //return response 
        if ($data == true) {
          return response()->json(['status'=>'sukses']);
        }else{
          return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
      $check = DB::table('d_site')->where('s_id',$id)->delete();

      if ($check == true) {
        return response()->json(['status'=>'sukses']);
        }else{
          return response()->json(['status'=>'gagal']); 
      }
    }
}
