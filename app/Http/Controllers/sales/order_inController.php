<?php

namespace App\Http\Controllers\sales;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;

class order_inController extends Controller
{

    public function index()
    {
       
        $sales = DB::table('SIF_Sales')->get();
        return view('sales.order_in.index',compact('nota','barang','customer','sales'));
    }
    public function create()
    {

        $code = DB::table('SALES_SO')->get();
        // return count($code);
        $itung = count($code);
        if ($itung == null) {
            $itung = 1;
        }
        else{
            $itung +=1;   
        }
        $nota = 'SO-'.date('ym').$itung;
        $barang = DB::table('SIF_Barang')->get();
        $gudang = DB::table('SIF_Gudang')->get();
        $customer = DB::table('SIF_Customer')->get();
        $sales = DB::table('SIF_Sales')->get();
        return view('sales.order_in.form',compact('gudang','barang','customer','sales'));
    }
    public function quotation_search(Request $request)
    {
        dd($request->all());
    }
    public function save(Request $request)
    {

    	//get all name/value
        // return $input = $request->all();
    
        if ($request->ppn == 'on') {
            $ppn = 'Y';
        }else{
            $ppn = 'N';
        }

        $code = DB::table('SALES_SO')->get();
        $hitung = count($code);
        if ($hitung == null) {
            $hitung = 1;
        }
        else{
            $hitung +=1;   
        }
        $nota = 'SO-'.date('ym').$hitung;

        $data = DB::table('SALES_SO')->insert([
            'Kd_Cabang'=>1,
            'Tipe_trans'=>1,
            'No_sp'=>$nota,
            'Atas_Nama'=>$request->atas_nama,
            'Kd_Customer'=>$request->customer,
            'Kd_sales'=>$request->sales,
            'Tgl_sp'=>$request->tgl_po,
            'Tgl_Kirim'=>$request->tgl_kirim,
            'Jenis_sp'=>$request->jenis_order,
            'SP_REFF'=>$request->spp_ref,
            'Flag_Ppn'=>$ppn,
        ]);
        for ($i=0; $i <count($request->ite_name) ; $i++) { 
            $data2[$i] = DB::table('SALES_SO_D')->insert([
            'Kd_Cabang'=>1,
            'No_seq'=>$i+1,
            'No_sp'=>$nota,
            'Deskripsi'=>$request->deskipsi[$i],
            'Qty'=>$request->qty[$i],
            'Last_create_date'=>date('Y-m-d'),
            'harga'=>$request->harga[$i],
            'Kd_Stok'=>$request->ite_name[$i],
            'Kd_satuan'=>$request->satuan[$i]
            ]);
        }

        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
    	$data = DB::table('d_site')->where('s_id',$id)->first();

        return view('master.master_site.edit',compact('data'));
    }
    public function update(Request $request)
    {
    	//get all name/value
        $input = $request->except('s_id');
    	//check unique row , if exist == 1
    	// $check = DB::table('d_site')->where('r_level',$request->r_level)->count();
    	$check = DB::table('d_site')
                        ->where('s_id',$request->s_id)
                        ->first();

        if ($check != null) {
            if ($check->s_id != $request->s_id) {
                return response()->json(['status'=>'ada']);
            }
        }
    	//save data
        $data = d_site::where('s_id', $request->s_id)->update($input);
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
    	$check = DB::table('d_site')->where('s_id',$id)->delete();

    	if ($check == true) {
    		return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);	
    	}
    }
}
