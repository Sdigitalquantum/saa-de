<?php

namespace App\Http\Controllers\sales;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;

class rencana_kirimController extends Controller
{

    public function index()
    {
        
        $data = DB::table('SALES_SO')->get();
        // if ($code == null) {
        //     $code = 1;
        // }
        // else{
        //     $code +=1;   
        // }
        // return 'a';
        $nota = 'SO-'.date('ym').'000'.'1';
        return view('sales.rencana_kirim.index',compact('nota','data'));
    }
    public function create($id)
    {
        // $code = DB::table('d_rencana_kirim')->max('dr_id');
        // if ($code == null) {
        //     $code = 1;
        // }
        // else{
        //     $code +=1;   
        // }
        $datahead = DB::table('SALES_SO')->where('No_sp','=',$id)->first();

        $datagudang = DB::table('INV_STOK_GUDANG as ISG')
                        ->join('SIF_Barang as B','B.Kode_Barang','=','ISG.kd_stok')
                        ->join('SIF_Gudang as G','G.Kode_Gudang','=','ISG.kode_gudang')
                        ->get();
        // return json_encode($datahead);
        $datadetil = DB::table('SALES_SO_D as SOD')
                        ->select('Kd_Stok','No_sp','awal_qty','qty_in','Nama_Barang','qty_lain_out','Qty','akhir_qty')
                        ->join('SIF_Barang as B','B.Kode_Barang','=','SOD.Kd_Stok')
                        ->leftjoin('INV_STOK_GUDANG as SG','SOD.Kd_Stok','=','SG.kd_stok')
                        ->where('No_sp','=',$id)
                        ->get();
        // return $datadetil;
        // return $datagudang;
        $nota = 'SO-'.date('ym').'000'.'1';
        return view('sales.rencana_kirim.form',compact('nota','datahead','datadetil','datagudang'));
    }
    public function check_gudang(Request $request)
    {
        $data = DB::table('INV_STOK_GUDANG')->where('kd_stok',$request->id)->get();
        $no_sp = DB::table('SALES_SO')->select('No_sp')->where('No_sp','=',$request->ns)->first();
        // return json_encode($no_sp);
        // return $no_sp;
        // return response()->json(['gudang'=>$data,'no_sp'=>$no_sp]);
        return view('sales.rencana_kirim.ajax_detail_gudang',compact('data','no_sp'));
    }
    public function save(Request $request)
    {

      //get all name/value
        // return $input = $request->all();
            $i = 0;
        // for ($i=0; $i <count($request->no_sp_id) ; $i++) { 
            $data = DB::table('PROD_rcn_krm_temp')->insert([
                'no_sp'=>$request->no_sp_id/*[$i]*/,
                'no_sp_dtl'=>$request->batch/*[$i]*/,
                'no_seq'=>$i+=1,
                'kd_barang'=>$request->kode_barang/*[$i]*/,
                'jumlah'=>$request->qty_kirim/*[$i]*/,
                'kd_gudang'=>$request->gudang/*[$i]*/,
            ]);
        // }
        
        

        if ($data == true) {
          return response()->json(['status'=>'sukses']);
        }else{
          return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
      $data = DB::table('d_site')->where('s_id',$id)->first();

        return view('master.master_site.edit',compact('data'));
    }
    public function update(Request $request)
    {
      //get all name/value
        $input = $request->except('s_id');
      //check unique row , if exist == 1
      // $check = DB::table('d_site')->where('r_level',$request->r_level)->count();
      $check = DB::table('d_site')
                        ->where('s_id',$request->s_id)
                        ->first();

        if ($check != null) {
            if ($check->s_id != $request->s_id) {
                return response()->json(['status'=>'ada']);
            }
        }
      //save data
        $data = d_site::where('s_id', $request->s_id)->update($input);
        //return response 
        if ($data == true) {
          return response()->json(['status'=>'sukses']);
        }else{
          return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
      $check = DB::table('d_site')->where('s_id',$id)->delete();

      if ($check == true) {
        return response()->json(['status'=>'sukses']);
        }else{
          return response()->json(['status'=>'gagal']); 
      }
    }
}
