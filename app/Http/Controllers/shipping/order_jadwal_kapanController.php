<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\d_mem;
use App\d_site;
use App\d_unit;

class master_userController extends Controller
{

    public function index()
    {

    	$data = DB::table('d_mem')->get();
      $menu = DB::table('d_menu')->get();

        return view('master.master_user.index',compact('data','menu'));
    }
    public function create()
    {
        $site = d_site::all();
        $unit = d_unit::all();
        $menu = DB::table('d_menu')->get();
        $access = DB::table('d_role')->get();
        return view('master.master_user.create',compact('site','unit','access','menu'));
    }
    public function save(Request $request)
    {

    	//get all name/value
    	   $save = new d_mem;
           $save->m_id=$request->m_id;
           $save->m_code=$request->m_id;
           $save->m_username=$request->m_username;
           $save->m_password=sha1(md5('لا إله إلاّ الله') . $request->m_password);
           $save->m_name=$request->m_name;
           $save->m_email=$request->m_email;
           // $save->m_birth_date=date('Y-m-d',strtotime($request->m_birth_date));
           // $save->m_address=$request->m_address;
           // $save->m_img=$request->m_img;
           $save->m_lastlogin=$request->m_lastlogin;
           $save->m_lastlogout=$request->m_lastlogout;
           $save->m_created_at=$request->m_created_at;
           $save->m_updated_at=$request->m_updated_at;
           // $save->m_birth_place=$request->m_birth_place;
           // $save->m_gender=$request->m_gender;
           $save->m_status=$request->m_status;
           $save->m_site=$request->m_site;
           $save->m_unit=$request->m_unit;
           $save->m_access=$request->m_unit;
           $save->save();
        
        //return response 
        if ($save == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }

    }
    public function edit($id)
    {
        $menu = DB::table('d_menu')->get();
    	 $data = DB::table('d_mem')->where('m_id',$id)->first();

        return view('master.master_user.edit',compact('data','menu'));
    }
    public function update(Request $request)
    {
    	//get all name/value
        $input = $request->except('m_id');
    	//check unique row , if exist == 1
    	// $check = DB::table('d_mem')->where('r_level',$request->r_level)->count();
    	// $check = DB::table('d_mem')
     //                    ->where('m_id',$request->m_id)
     //                    ->first();

     //    if ($check != null) {
     //        if ($check->m_id != $request->m_id) {
     //            return response()->json(['status'=>'ada']);
     //        }
     //    }
    	//save data
        $data = d_mem::where('m_id', $request->m_id)->update($input);
        //return response 
        if ($data == true) {
        	return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);
        }
    }
    public function delete($id)
    {
    	$check = DB::table('d_mem')->where('m_id',$id)->delete();

    	if ($check == true) {
    		return response()->json(['status'=>'sukses']);
        }else{
        	return response()->json(['status'=>'gagal']);	
    	}
    }
}
