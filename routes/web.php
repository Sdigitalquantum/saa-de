<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//MASTER
   //MASTER CUSTOMER
   Route::get('/master/master_customer', 'master\master_customerController@index')->name('master_customer');
   Route::get('/master/master_customer/create', 'master\master_customerController@create')->name('master_customer_create');
   Route::get('/master/master_customer/save', 'master\master_customerController@save')->name('master_customer_save');
   Route::get('/master/master_customer/edit/{id}', 'master\master_customerController@edit')->name('master_customer_edit');
   Route::get('/master/master_customer/update/{id}', 'master\master_customerController@update')->name('master_customer_update');
   Route::get('/master/master_customer/delete/{id}', 'master\master_customerController@delete')->name('master_customer_delete');
   
   //MASTER SUPPLIER
   Route::get('/master/master_supplier', 'master\master_supplierController@index')->name('master_supplier');
   Route::get('/master/master_supplier/create', 'master\master_supplierController@create')->name('master_supplier_create');
   Route::get('/master/master_supplier/save', 'master\master_supplierController@save')->name('master_supplier_save');
   Route::get('/master/master_supplier/edit/{id}', 'master\master_supplierController@edit')->name('master_supplier_edit');
   Route::get('/master/master_supplier/update/{id}', 'master\master_supplierController@update')->name('master_supplier_update');
   Route::get('/master/master_supplier/delete/{id}', 'master\master_supplierController@delete')->name('master_supplier_delete');   

   //MASTER MATA UANG
   Route::get('/master/master_mata_uang', 'master\master_mata_uangController@index')->name('master_mata_uang');
   Route::get('/master/master_mata_uang/create', 'master\master_mata_uangController@create')->name('master_mata_uang_create');
   Route::get('/master/master_mata_uang/save', 'master\master_mata_uangController@save')->name('master_mata_uang_save');
   Route::get('/master/master_mata_uang/edit/{id}', 'master\master_mata_uangController@edit')->name('master_mata_uang_edit');
   Route::get('/master/master_mata_uang/update/{id}', 'master\master_mata_uangController@update')->name('master_mata_uang_update');
   Route::get('/master/master_mata_uang/delete/{id}', 'master\master_mata_uangController@delete')->name('master_mata_uang_delete');  

   //MASTER BARANG
   Route::get('/master/master_barang', 'master\master_barangController@index')->name('master_barang');
   Route::get('/master/master_barang/create', 'master\master_barangController@create')->name('master_barang_create');
   Route::get('/master/master_barang/save', 'master\master_barangController@save')->name('master_barang_save');
   Route::get('/master/master_barang/edit/{id}', 'master\master_barangController@edit')->name('master_barang_edit');
   Route::get('/master/master_barang/update/{id}', 'master\master_barangController@update')->name('master_barang_update');
   Route::get('/master/master_barang/delete/{id}', 'master\master_barangController@delete')->name('master_barang_delete');   

   //MASTER GUDANG
   Route::get('/master/master_gudang', 'master\master_gudangController@index')->name('master_gudang');
   Route::get('/master/master_gudang/create', 'master\master_gudangController@create')->name('master_gudang_create');
   Route::get('/master/master_gudang/save', 'master\master_gudangController@save')->name('master_gudang_save');
   Route::get('/master/master_gudang/edit/{id}', 'master\master_gudangController@edit')->name('master_gudang_edit');
   Route::get('/master/master_gudang/update/{id}', 'master\master_gudangController@update')->name('master_gudang_update');
   Route::get('/master/master_gudang/delete/{id}', 'master\master_gudangController@delete')->name('master_gudang_delete');   

   //MASTER COA
   Route::get('/master/master_coa', 'master\master_coaController@index')->name('master_coa');
   Route::get('/master/master_coa/create', 'master\master_coaController@create')->name('master_coa_create');
   Route::get('/master/master_coa/save', 'master\master_coaController@save')->name('master_coa_save');
   Route::get('/master/master_coa/edit/{id}', 'master\master_coaController@edit')->name('master_coa_edit');
   Route::get('/master/master_coa/update/{id}', 'master\master_coaController@update')->name('master_coa_update');
   Route::get('/master/master_coa/delete/{id}', 'master\master_coaController@delete')->name('master_coa_delete');   

   //MASTER JENIS POS BIAYA
   Route::get('/master/master_jenis_pos_biaya', 'master\master_jenis_pos_biayaController@index')->name('master_jenis_pos_biaya');
   Route::get('/master/master_jenis_pos_biaya/create', 'master\master_jenis_pos_biayaController@create')->name('master_jenis_pos_biaya_create');
   Route::get('/master/master_jenis_pos_biaya/save', 'master\master_jenis_pos_biayaController@save')->name('master_jenis_pos_biaya_save');
   Route::get('/master/master_jenis_pos_biaya/edit/{id}', 'master\master_jenis_pos_biayaController@edit')->name('master_jenis_pos_biaya_edit');
   Route::get('/master/master_jenis_pos_biaya/update/{id}', 'master\master_jenis_pos_biayaController@update')->name('master_jenis_pos_biaya_update');
   Route::get('/master/master_jenis_pos_biaya/delete/{id}', 'master\master_jenis_pos_biayaController@delete')->name('master_jenis_pos_biaya_delete');


//inventory
   //quality_control
   Route::get('/inventory/quality_control', 'inventory\quality_controlController@index')->name('quality_control');
   Route::get('/inventory/quality_control/create', 'inventory\quality_controlController@create')->name('quality_control_create');
   Route::get('/inventory/quality_control/save', 'inventory\quality_controlController@save')->name('quality_control_save');
   Route::get('/inventory/quality_control/edit/{id}', 'inventory\quality_controlController@edit')->name('quality_control_edit');
   Route::get('/inventory/quality_control/update/{id}', 'inventory\quality_controlController@update')->name('quality_control_update');
   Route::get('/inventory/quality_control/delete/{id}', 'inventory\quality_controlController@delete')->name('quality_control_delete');

   //barang_in
   Route::get('/inventory/barang_in', 'inventory\barang_inController@index')->name('barang_in');
   Route::get('/inventory/barang_in/create', 'inventory\barang_inController@create')->name('barang_in_create');
   Route::get('/inventory/barang_in/save', 'inventory\barang_inController@save')->name('barang_in_save');
   Route::get('/inventory/barang_in/edit/{id}', 'inventory\barang_inController@edit')->name('barang_in_edit');
   Route::get('/inventory/barang_in/update/{id}', 'inventory\barang_inController@update')->name('barang_in_update');
   Route::get('/inventory/barang_in/delete/{id}', 'inventory\barang_inController@delete')->name('barang_in_delete');

   //barang_out
   Route::get('/inventory/barang_out', 'inventory\barang_outController@index')->name('barang_out');
   Route::get('/inventory/barang_out/create', 'inventory\barang_outController@create')->name('barang_out_create');
   Route::get('/inventory/barang_out/save', 'inventory\barang_outController@save')->name('barang_out_save');
   Route::get('/inventory/barang_out/edit/{id}', 'inventory\barang_outController@edit')->name('barang_out_edit');
   Route::get('/inventory/barang_out/update/{id}', 'inventory\barang_outController@update')->name('barang_out_update');
   Route::get('/inventory/barang_out/delete/{id}', 'inventory\barang_outController@delete')->name('barang_out_delete');

   //pindah_gudang
   Route::get('/inventory/pindah_gudang', 'inventory\pindah_gudangController@index')->name('pindah_gudang');
   Route::get('/inventory/pindah_gudang/create', 'inventory\pindah_gudangController@create')->name('pindah_gudang_create');
   Route::get('/inventory/pindah_gudang/save', 'inventory\pindah_gudangController@save')->name('pindah_gudang_save');
   Route::get('/inventory/pindah_gudang/edit/{id}', 'inventory\pindah_gudangController@edit')->name('pindah_gudang_edit');
   Route::get('/inventory/pindah_gudang/update/{id}', 'inventory\pindah_gudangController@update')->name('pindah_gudang_update');
   Route::get('/inventory/pindah_gudang/delete/{id}', 'inventory\pindah_gudangController@delete')->name('pindah_gudang_delete');

   //ganti_wujud
   Route::get('/inventory/ganti_wujud', 'inventory\ganti_wujudController@index')->name('ganti_wujud');
   Route::get('/inventory/ganti_wujud/create', 'inventory\ganti_wujudController@create')->name('ganti_wujud_create');
   Route::get('/inventory/ganti_wujud/save', 'inventory\ganti_wujudController@save')->name('ganti_wujud_save');
   Route::get('/inventory/ganti_wujud/edit/{id}', 'inventory\ganti_wujudController@edit')->name('ganti_wujud_edit');
   Route::get('/inventory/ganti_wujud/update/{id}', 'inventory\ganti_wujudController@update')->name('ganti_wujud_update');
   Route::get('/inventory/ganti_wujud/delete/{id}', 'inventory\ganti_wujudController@delete')->name('ganti_wujud_delete');


// --------------------------------------------------------------------------------------------------------------------

//purchase
   //SALES
   Route::get('/purchase/purchase_request', 'purchase\purchase_requestController@index')->name('purchase_request');
   Route::get('/purchase/purchase_request/create', 'purchase\purchase_requestController@create')->name('purchase_request_create');
   Route::get('/purchase/purchase_request/save', 'purchase\purchase_requestController@save')->name('purchase_request_save');
   Route::get('/purchase/purchase_request/edit/{id}', 'purchase\purchase_requestController@edit')->name('purchase_request_edit');
   Route::get('/purchase/purchase_request/update/{id}', 'purchase\purchase_requestController@update')->name('purchase_request_update');
   Route::get('/purchase/purchase_request/delete/{id}', 'purchase\purchase_requestController@delete')->name('purchase_request_delete');

   // confirm acc purchase request
   Route::get('/purchase/purchase_request_acc', 'purchase\purchase_requestController@index_acc')->name('purchase_request_acc');
   Route::get('/purchase/purchase_request_acc/save/{id}', 'purchase\purchase_requestController@save_acc')->name('purchase_request_save_acc');

   //rencana_kirim
   Route::get('/purchase/purchase_order', 'purchase\purchase_orderController@index')->name('purchase_order');
   Route::get('/purchase/purchase_order/create', 'purchase\purchase_orderController@create')->name('purchase_order_create');
   Route::get('/purchase/purchase_order/cari/{id}', 'purchase\purchase_orderController@cari')->name('purchase_order_cari');
   Route::get('/purchase/purchase_order/save', 'purchase\purchase_orderController@save')->name('purchase_order_save');
   Route::get('/purchase/purchase_order/edit/{id}', 'purchase\purchase_orderController@edit')->name('purchase_order_edit');
   Route::get('/purchase/purchase_order/update/{id}', 'purchase\purchase_orderController@update')->name('purchase_order_update');
   Route::get('/purchase/purchase_order/delete/{id}', 'purchase\purchase_orderController@delete')->name('purchase_request_delete');


   // confirm acc purchase order
   Route::get('/purchase/purchase_order_acc', 'purchase\purchase_orderController@index_acc')->name('purchase_order_acc');
   Route::get('/purchase/purchase_order_acc/save/{id}', 'purchase\purchase_orderController@save_acc')->name('purchase_order_save_acc');


   //order
   Route::get('/purchase/retur', 'purchase\returController@index')->name('retur');
   Route::get('/purchase/retur/create', 'purchase\returController@create')->name('retur_create');
   Route::get('/purchase/retur/save', 'purchase\returController@save')->name('retur_save');
   Route::get('/purchase/retur/edit/{id}', 'purchase\returController@edit')->name('retur_edit');
   Route::get('/purchase/retur/update/{id}', 'purchase\returController@update')->name('retur_update');
   Route::get('/purchase/retur/delete/{id}', 'purchase\returController@delete')->name('retur_delete');


// --------------------------------------------------------------------------------------------------------------------

//SALES
   //SALES
   Route::get('/sales/order_in', 'sales\order_inController@index')->name('order_in');
   Route::get('/sales/order_in/create', 'sales\order_inController@create')->name('order_in_create');
   Route::get('/sales/order_in/quotation_search', 'sales\order_inController@quotation_search')->name('quotation_search');
   Route::get('/sales/order_in/save', 'sales\order_inController@save')->name('order_in_save');
   Route::get('/sales/order_in/edit/{id}', 'sales\order_inController@edit')->name('order_in_edit');
   Route::get('/sales/order_in/update/{id}', 'sales\order_inController@update')->name('order_in_update');
   Route::get('/sales/order_in/delete/{id}', 'sales\order_inController@delete')->name('order_in_delete');

   //rencana_kirim
   Route::get('/sales/rencana_kirim', 'sales\rencana_kirimController@index')->name('rencana_kirim');
   Route::get('/sales/rencana_kirim/create/{id}', 'sales\rencana_kirimController@create')->name('rencana_kirim_create');
   Route::get('/sales/rencana_kirim/check_gudang', 'sales\rencana_kirimController@check_gudang')->name('rencana_kirim_check_gudang');
   Route::get('/sales/rencana_kirim/save', 'sales\rencana_kirimController@save')->name('rencana_kirim_save');
   Route::get('/sales/rencana_kirim/edit/{id}', 'sales\rencana_kirimController@edit')->name('rencana_kirim_edit');
   Route::get('/sales/rencana_kirim/update/{id}', 'sales\rencana_kirimController@update')->name('rencana_kirim_update');
   Route::get('/sales/rencana_kirim/delete/{id}', 'sales\rencana_kirimController@delete')->name('rencana_kirim_delete');

   //order
   Route::get('/sales/order', 'sales\orderController@index')->name('order');
   Route::get('/sales/order/create/{id}', 'sales\orderController@create')->name('order_create');
   Route::get('/sales/order/save', 'sales\orderController@save')->name('order_save');
   Route::get('/sales/order/edit/{id}', 'sales\orderController@edit')->name('order_edit');
   Route::get('/sales/order/update/{id}', 'sales\orderController@update')->name('order_update');
   Route::get('/sales/order/delete/{id}', 'sales\orderController@delete')->name('order_delete');

   //order
   Route::get('/sales/proforma_invoice', 'sales\proforma_invoiceController@index')->name('proforma_invoice');
   Route::get('/sales/proforma_invoice/create', 'sales\proforma_invoiceController@create')->name('proforma_invoice_create');
   Route::get('/sales/proforma_invoice/save', 'sales\proforma_invoiceController@save')->name('proforma_invoice_save');
   Route::get('/sales/proforma_invoice/edit/{id}', 'sales\proforma_invoiceController@edit')->name('proforma_invoice_edit');
   Route::get('/sales/proforma_invoice/update/{id}', 'sales\proforma_invoiceController@update')->name('proforma_invoice_update');
   Route::get('/sales/proforma_invoice/delete/{id}', 'sales\proforma_invoiceController@delete')->name('proforma_invoice_delete');

   //order
   Route::get('/sales/surat_jalan', 'sales\surat_jalanController@index')->name('surat_jalan');
   Route::get('/sales/surat_jalan/create', 'sales\surat_jalanController@create')->name('surat_jalan_create');
   Route::get('/sales/surat_jalan/save', 'sales\surat_jalanController@save')->name('surat_jalan_save');
   Route::get('/sales/surat_jalan/edit/{id}', 'sales\surat_jalanController@edit')->name('surat_jalan_edit');
   Route::get('/sales/surat_jalan/update/{id}', 'sales\surat_jalanController@update')->name('surat_jalan_update');
   Route::get('/sales/surat_jalan/delete/{id}', 'sales\surat_jalanController@delete')->name('surat_jalan_delete');

   //order
   Route::get('/sales/confirm_sj', 'sales\confirm_sjController@index')->name('confirm_sj');
   Route::get('/sales/confirm_sj/create', 'sales\confirm_sjController@create')->name('confirm_sj_create');
   Route::get('/sales/confirm_sj/save', 'sales\confirm_sjController@save')->name('confirm_sj_save');
   Route::get('/sales/confirm_sj/edit/{id}', 'sales\confirm_sjController@edit')->name('confirm_sj_edit');
   Route::get('/sales/confirm_sj/update/{id}', 'sales\confirm_sjController@update')->name('confirm_sj_update');
   Route::get('/sales/confirm_sj/delete/{id}', 'sales\confirm_sjController@delete')->name('confirm_sj_delete');

   //order
   Route::get('/sales/reroute_sj', 'sales\reroute_sjController@index')->name('reroute_sj');
   Route::get('/sales/reroute_sj/create', 'sales\reroute_sjController@create')->name('reroute_sj_create');
   Route::get('/sales/reroute_sj/save', 'sales\reroute_sjController@save')->name('reroute_sj_save');
   Route::get('/sales/reroute_sj/edit/{id}', 'sales\reroute_sjController@edit')->name('reroute_sj_edit');
   Route::get('/sales/reroute_sj/update/{id}', 'sales\reroute_sjController@update')->name('reroute_sj_update');
   Route::get('/sales/reroute_sj/delete/{id}', 'sales\reroute_sjController@delete')->name('reroute_sj_delete');

   //order
   Route::get('/sales/cansel_closing', 'sales\cansel_closingController@index')->name('cansel_closing');
   Route::get('/sales/cansel_closing/create', 'sales\cansel_closingController@create')->name('cansel_closing_create');
   Route::get('/sales/cansel_closing/save', 'sales\cansel_closingController@save')->name('cansel_closing_save');
   Route::get('/sales/cansel_closing/edit/{id}', 'sales\cansel_closingController@edit')->name('cansel_closing_edit');
   Route::get('/sales/cansel_closing/update/{id}', 'sales\cansel_closingController@update')->name('cansel_closing_update');
   Route::get('/sales/cansel_closing/delete/{id}', 'sales\cansel_closingController@delete')->name('cansel_closing_delete');

// --------------------------------------------------------------------------------------------------------------------

//sHIPPING
   //SALES
   Route::get('/shipping/order_jadwal_kapan', 'shipping\order_jadwal_kapanController@index')->name('order_jadwal_kapan');
   Route::get('/shipping/order_jadwal_kapan/create', 'shipping\order_jadwal_kapanController@create')->name('order_jadwal_kapan_create');
   Route::get('/shipping/order_jadwal_kapan/save', 'shipping\order_jadwal_kapanController@save')->name('order_jadwal_kapan_save');
   Route::get('/shipping/order_jadwal_kapan/edit/{id}', 'shipping\order_jadwal_kapanController@edit')->name('order_jadwal_kapan_edit');
   Route::get('/shipping/order_jadwal_kapan/update/{id}', 'shipping\order_jadwal_kapanController@update')->name('order_jadwal_kapan_update');
   Route::get('/shipping/order_jadwal_kapan/delete/{id}', 'shipping\order_jadwal_kapanController@delete')->name('order_jadwal_kapan_delete');

   //rencana_kirim
   Route::get('/shipping/konfirmasi_order', 'shipping\konfirmasi_orderController@index')->name('konfirmasi_order');
   Route::get('/shipping/konfirmasi_order/create', 'shipping\konfirmasi_orderController@create')->name('konfirmasi_order_create');
   Route::get('/shipping/konfirmasi_order/save', 'shipping\konfirmasi_orderController@save')->name('konfirmasi_order_save');
   Route::get('/shipping/konfirmasi_order/edit/{id}', 'shipping\konfirmasi_orderController@edit')->name('konfirmasi_order_edit');
   Route::get('/shipping/konfirmasi_order/update/{id}', 'shipping\konfirmasi_orderController@update')->name('konfirmasi_order_update');
   Route::get('/shipping/konfirmasi_order/delete/{id}', 'shipping\konfirmasi_orderController@delete')->name('konfirmasi_order_delete');

   //order
   Route::get('/shipping/stuffing', 'shipping\stuffingController@index')->name('stuffing');
   Route::get('/shipping/stuffing/create', 'shipping\stuffingController@create')->name('stuffing_create');
   Route::get('/shipping/stuffing/save', 'shipping\stuffingController@save')->name('stuffing_save');
   Route::get('/shipping/stuffing/edit/{id}', 'shipping\stuffingController@edit')->name('stuffing_edit');
   Route::get('/shipping/stuffing/update/{id}', 'shipping\stuffingController@update')->name('stuffing_update');
   Route::get('/shipping/stuffing/delete/{id}', 'shipping\stuffingController@delete')->name('stuffing_delete');

   //order
   Route::get('/shipping/hasil_timbang', 'shipping\hasil_timbangController@index')->name('hasil_timbang');
   Route::get('/shipping/hasil_timbang/create', 'shipping\hasil_timbangController@create')->name('hasil_timbang_create');
   Route::get('/shipping/hasil_timbang/save', 'shipping\hasil_timbangController@save')->name('hasil_timbang_save');
   Route::get('/shipping/hasil_timbang/edit/{id}', 'shipping\hasil_timbangController@edit')->name('hasil_timbang_edit');
   Route::get('/shipping/hasil_timbang/update/{id}', 'shipping\hasil_timbangController@update')->name('hasil_timbang_update');
   Route::get('/shipping/hasil_timbang/delete/{id}', 'shipping\hasil_timbangController@delete')->name('hasil_timbang_delete');

   //order
   Route::get('/shipping/invoice_packlist', 'shipping\invoice_packlistController@index')->name('invoice_packlist');
   Route::get('/shipping/invoice_packlist/create', 'shipping\invoice_packlistController@create')->name('invoice_packlist_create');
   Route::get('/shipping/invoice_packlist/save', 'shipping\invoice_packlistController@save')->name('invoice_packlist_save');
   Route::get('/shipping/invoice_packlist/edit/{id}', 'shipping\invoice_packlistController@edit')->name('invoice_packlist_edit');
   Route::get('/shipping/invoice_packlist/update/{id}', 'shipping\invoice_packlistController@update')->name('invoice_packlist_update');
   Route::get('/shipping/invoice_packlist/delete/{id}', 'shipping\invoice_packlistController@delete')->name('invoice_packlist_delete');

   //order
   Route::get('/shipping/dokumen', 'shipping\dokumenController@index')->name('dokumen');
   Route::get('/shipping/dokumen/create', 'shipping\dokumenController@create')->name('dokumen_create');
   Route::get('/shipping/dokumen/save', 'shipping\dokumenController@save')->name('dokumen_save');
   Route::get('/shipping/dokumen/edit/{id}', 'shipping\dokumenController@edit')->name('dokumen_edit');
   Route::get('/shipping/dokumen/update/{id}', 'shipping\dokumenController@update')->name('dokumen_update');
   Route::get('/shipping/dokumen/delete/{id}', 'shipping\dokumenController@delete')->name('dokumen_delete');

   //order
   Route::get('/shipping/invoice', 'shipping\invoiceController@index')->name('invoice');
   Route::get('/shipping/invoice/create', 'shipping\invoiceController@create')->name('invoice_create');
   Route::get('/shipping/invoice/save', 'shipping\invoiceController@save')->name('invoice_save');
   Route::get('/shipping/invoice/edit/{id}', 'shipping\invoiceController@edit')->name('invoice_edit');
   Route::get('/shipping/invoice/update/{id}', 'shipping\invoiceController@update')->name('invoice_update');
   Route::get('/shipping/invoice/delete/{id}', 'shipping\invoiceController@delete')->name('invoice_delete');


// --------------------------------------------------------------------------------------------------------------------

//FINANCE
   //SALES
   Route::get('/finance/hutang', 'finance\hutangController@index')->name('hutang');
   Route::get('/finance/hutang/create', 'finance\hutangController@create')->name('hutang_create');
   Route::get('/finance/hutang/save', 'finance\hutangController@save')->name('hutang_save');
   Route::get('/finance/hutang/edit/{id}', 'finance\hutangController@edit')->name('hutang_edit');
   Route::get('/finance/hutang/update/{id}', 'finance\hutangController@update')->name('hutang_update');
   Route::get('/finance/hutang/delete/{id}', 'finance\hutangController@delete')->name('hutang_delete');

   //rencana_kirim
   Route::get('/finance/piutang', 'finance\piutangController@index')->name('piutang');
   Route::get('/finance/piutang/create', 'finance\piutangController@create')->name('piutang_create');
   Route::get('/finance/piutang/save', 'finance\piutangController@save')->name('piutang_save');
   Route::get('/finance/piutang/edit/{id}', 'finance\piutangController@edit')->name('piutang_edit');
   Route::get('/finance/piutang/update/{id}', 'finance\piutangController@update')->name('piutang_update');
   Route::get('/finance/piutang/delete/{id}', 'finance\piutangController@delete')->name('piutang_delete');

   //order
   Route::get('/finance/kas', 'finance\kasController@index')->name('kas');
   Route::get('/finance/kas/create', 'finance\kasController@create')->name('kas_create');
   Route::get('/finance/kas/save', 'finance\kasController@save')->name('kas_save');
   Route::get('/finance/kas/edit/{id}', 'finance\kasController@edit')->name('kas_edit');
   Route::get('/finance/kas/update/{id}', 'finance\kasController@update')->name('kas_update');
   Route::get('/finance/kas/delete/{id}', 'finance\kasController@delete')->name('kas_delete');

   //order
   Route::get('/finance/general_ledger', 'finance\general_ledgerController@index')->name('general_ledger');
   Route::get('/finance/general_ledger/create', 'finance\general_ledgerController@create')->name('general_ledger_create');
   Route::get('/finance/general_ledger/save', 'finance\general_ledgerController@save')->name('general_ledger_save');
   Route::get('/finance/general_ledger/edit/{id}', 'finance\general_ledgerController@edit')->name('general_ledger_edit');
   Route::get('/finance/general_ledger/update/{id}', 'finance\general_ledgerController@update')->name('general_ledger_update');
   Route::get('/finance/general_ledger/delete/{id}', 'finance\general_ledgerController@delete')->name('general_ledger_delete');

 // --------------------------------------------------------------------------------------------------------------------

//MONITORUNG
   //SALES
   Route::get('/monitoring/monitoring_sales', 'monitoring\monitoring_salesController@index')->name('monitoring_sales');
   Route::get('/monitoring/monitoring_sales/create', 'monitoring\monitoring_salesController@create')->name('monitoring_sales_create');
   Route::get('/monitoring/monitoring_sales/save', 'monitoring\monitoring_salesController@save')->name('monitoring_sales_save');
   Route::get('/monitoring/monitoring_sales/edit/{id}', 'monitoring\monitoring_salesController@edit')->name('monitoring_sales_edit');
   Route::get('/monitoring/monitoring_sales/update/{id}', 'monitoring\monitoring_salesController@update')->name('monitoring_sales_update');
   Route::get('/monitoring/monitoring_sales/delete/{id}', 'monitoring\monitoring_salesController@delete')->name('monitoring_sales_delete');

   //rencana_kirim
   Route::get('/monitoring/monitoring_purchase', 'monitoring\monitoring_purchaseController@index')->name('monitoring_purchase');
   Route::get('/monitoring/monitoring_purchase/create', 'monitoring\monitoring_purchaseController@create')->name('monitoring_purchase_create');
   Route::get('/monitoring/monitoring_purchase/save', 'monitoring\monitoring_purchaseController@save')->name('monitoring_purchase_save');
   Route::get('/monitoring/monitoring_purchase/edit/{id}', 'monitoring\monitoring_purchaseController@edit')->name('monitoring_purchase_edit');
   Route::get('/monitoring/monitoring_purchase/update/{id}', 'monitoring\monitoring_purchaseController@update')->name('monitoring_purchase_update');
   Route::get('/monitoring/monitoring_purchase/delete/{id}', 'monitoring\monitoring_purchaseController@delete')->name('monitoring_purchase_delete');


 // --------------------------------------------------------------------------------------------------------------------

//report
   //SALES
   Route::get('/report/report_sales', 'report\report_salesController@index')->name('report_sales');
   Route::get('/report/report_sales/create', 'report\report_salesController@create')->name('report_sales_create');
   Route::get('/report/report_sales/save', 'report\report_salesController@save')->name('report_sales_save');
   Route::get('/report/report_sales/edit/{id}', 'report\report_salesController@edit')->name('report_sales_edit');
   Route::get('/report/report_sales/update/{id}', 'report\report_salesController@update')->name('report_sales_update');
   Route::get('/report/report_sales/delete/{id}', 'report\report_salesController@delete')->name('report_sales_delete');

   //rencana_kirim
   Route::get('/report/report_purchase', 'report\report_purchaseController@index')->name('report_purchase');
   Route::get('/report/report_purchase/create', 'report\report_purchaseController@create')->name('report_purchase_create');
   Route::get('/report/report_purchase/save', 'report\report_purchaseController@save')->name('report_purchase_save');
   Route::get('/report/report_purchase/edit/{id}', 'report\report_purchaseController@edit')->name('report_purchase_edit');
   Route::get('/report/report_purchase/update/{id}', 'report\report_purchaseController@update')->name('report_purchase_update');
   Route::get('/report/report_purchase/delete/{id}', 'report\report_purchaseController@delete')->name('report_purchase_delete');
